package cib.digital.tests;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRetrieverSubBreedsTest {
			//Declares properties file globally
			Properties prop=new Properties();
		
	@BeforeTest
	public void getData() throws IOException
		{
			//Sets file location for properties file host address
			FileInputStream fis=new FileInputStream("src\\main\\java\\cib\\digital\\resources\\env.properties");
			prop.load(fis);
		
		}
		
	@Test
	public  void ListSubBreedRetriever() 
		{	
			RestAssured.proxy("proxy.ad.property24.com", 8080); //Set if behind company proxy
			RestAssured.baseURI=prop.getProperty("HOST");
			
			//Assigns the response to a variable
			Response res=
			given().     
			when().	
			
			//Sends request to retrieve list of all breeds
			get("api/breeds/list/all").
			
			//Verifies that the response is returned successfully and retriever breed is in the response
			then().assertThat().statusCode(200).and().contentType(ContentType.JSON).and().body("message.retriever[3]",equalTo("golden")).
			extract().response();
			
			//Converts response from Raw text to a String
			String responseString=res.asString();
			
			//Converts response String to JSON and prints the list of retriever sub-breed
			JsonPath js=new JsonPath(responseString);
			
			//Lists sub-breeds of retriever json object
			ArrayList subBreed=js.get("message.retriever");
			System.out.println(subBreed);
		}

}
