package cib.digital.tests;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Pattern;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class GetGoldenRetrieverImageTest {
			//Declares properties file globally
			Properties prop=new Properties();
		
	@BeforeTest
	public void getData() throws IOException
		{
			//Sets file location for properties file containing host address
			FileInputStream fis=new FileInputStream("src\\main\\java\\cib\\digital\\resources\\env.properties");
			prop.load(fis);
		
		}
		
	
	@Test
	public void GetRetrieverImage() 
		{
			RestAssured.proxy("proxy.ad.property24.com", 8080);//Set if behind company proxy
			RestAssured.baseURI=prop.getProperty("HOST");
			
			//Regular expression to match URL of random golden retriever image
			String pattern = "https:\\/\\/images.dog.ceo\\/breeds\\/retriever-golden\\/[n]\\d{8}_\\d*\\.\\w+";
			Pattern r = Pattern.compile(pattern);
			
			given().   
			when().	
			get("api/breed/retriever/golden/images/random").
			then().assertThat()
			//Verifies that the response is returned successfully.
			.body("status", equalTo("success"))
			//checks if the url in the response body matches the  regular expression
			.body("message",  matchesPattern(pattern));
			
			
		}

}